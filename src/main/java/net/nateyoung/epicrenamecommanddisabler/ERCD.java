package net.nateyoung.epicrenamecommanddisabler;

import net.nateyoung.epicrenamecommanddisabler.listener.OnCommandRun;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ERCD extends JavaPlugin {

    public void onEnable() {
        PluginManager pm = this.getServer().getPluginManager();

        pm.registerEvents(new OnCommandRun(), this);

    }

}
