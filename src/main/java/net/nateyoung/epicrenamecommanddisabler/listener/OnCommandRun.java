package net.nateyoung.epicrenamecommanddisabler.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.ArrayList;
import java.util.List;

public class OnCommandRun implements Listener {

    public static List<String> disabledCommands = new ArrayList<String>();

    static {
        disabledCommands.add("setlore");
        disabledCommands.add("glow");
        disabledCommands.add("removeglow");
        disabledCommands.add("rename");
        disabledCommands.add("setloreline");
        disabledCommands.add("removeloreline");
    }

    public void onCommandEvent(PlayerCommandPreprocessEvent event) {

        Material itemTypeInHand = event.getPlayer().getInventory().getItemInMainHand().getType();

        if (!event.getPlayer().hasPermission("ercd.override" ) &&
                (itemTypeInHand.equals(Material.PAPER) || itemTypeInHand.equals(Material.BOOK))) {

            String[] message = event.getMessage().split(" ");

            String command = message[0].replace("epicrename:", "");

            if (OnCommandRun.disabledCommands.contains(command)) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(ChatColor.RED + "You can't run that command on books and paper.");
            }


        }

    }

}
